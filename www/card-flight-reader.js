/**
 * Module for interaction with CardFlight card reader
 * Cordova plugin readme: https://github.com/pgdaniel/cordova_cardflight
 */
APP.factory('CardFlightReader', function($window, $q, State, $ionicPopup) {
    var errorPluginIsNotInstalled = {message: 'CardFlight plugin is not installed'};
    var errorNotAvailableAtWeb = {message: 'CardFlight plugin is not available at the web platform'};

    var CardFlightReader = {
        isConnected: false,
        isReady: false,

        cardFlight: null,

        init: function () {
            console.log('[CardFlightReader]#init()');
            CardFlightReader.cardFlight = $window.cordova && $window.cordova.plugins && $window.cordova.plugins.cardFlight;

            if (CardFlightReader.cardFlight) {
                console.log('[CardFlightReader]#init() CardFlight plugin was found, init authorization');
                CardFlightReader.cardFlight.authorize(
                    CONFIG.cardFlight.apiToken,
                    CONFIG.cardFlight.accountToken,
                    function onSuccess () {
                        console.log('[CardFlightReader] Succeed CardFlight authorize');
                    },
                    function onError () {
                        console.log('[CardFlightReader] Failed CardFlight authorize');
                    }
                );

                CardFlightReader.cardFlight.onReaderConnecting(function () {
                    console.log('[CardFlightReader]#Event: onReaderConnecting');
                });

                CardFlightReader.cardFlight.onBatteryLow(function () {
                    console.log('[CardFlightReader]#Event: onBatteryLow');
                });

                CardFlightReader.cardFlight.onReaderAttached(function () {
                    console.log('[CardFlightReader]#Event: onReaderAttached');
                });

                CardFlightReader.cardFlight.onReaderDisconnected(function () {

                    if(CardFlightReader.cardFlight) {
                        CardFlightReader.cardFlight.stopSwipe();
                        deferred.resolve();
                    }
                    console.log('[CardFlightReader]#Event: onReaderDisconnected');
                    CardFlightReader.isConnected = false;
                    CardFlightReader.isReady = false;

                    return $ionicPopup.alert({
                        title: "Card Reader Disconnected!",
                        template: "The card reader is disconnected! Please reconnect and resume.",
                        cssClass: "text-center"
                    });

                    //console.log('[CardFlightReader]#Status: isConnected = false, isReady = false');
                });


                CardFlightReader.cardFlight.onReaderConnected(function () {
                    //console.log('[CardFlightReader]#onReaderConnected hook success');
                    CardFlightReader.isConnected = true;
                    CardFlightReader.isReady = true;

                    console.log('[CardFlightReader]#Status: isConnected = true, isReady = true');
                }, function () {
                    console.log('[CardFlightReader]#onReaderConnected hook failed');
                });

                CardFlightReader.cardFlight.startReader(function () {
                    //console.log('[CardFlightReader] cardFlight.startReader callback');
                    CardFlightReader.isReady = true;
                    console.log('[CardFlightReader]#Status: isConnected = ' + CardFlightReader.isConnected + ', isReady = true');
                });
            }
            else if (window.platform == 'Web') {
                console.log('[CardFlightReader]#init() web platform');
                State.showMessage(errorPluginIsNotInstalled.message, ERROR_LEVELS.debug);
                console.log("swipe error", errorPluginIsNotInstalled.message);
            }
            else {
                console.log('[CardFlightReader]#init() plugin was not found');
                console.log(errorNotAvailableAtWeb.message);
            }
        },

        /**
         * @returns {Q.promse}
         */
        beginSwipe: function () {

            console.log('[CardFlightReader]#beginSwipe()');
            var deferred = $q.defer();

            if (CardFlightReader.cardFlight) {

                if (!CardFlightReader.isConnected) {
                    return
                }

                console.log('[CardFlightReader]#beginSwipe() plugin was found');

                if (!CardFlightReader.isReady) {
                    console.log('[CardFlightReader]#beginSwipe() not ready');

                    var msg = 'Reader is disconnected. Connect it before checkout';
                    State.showMessage(msg, ERROR_LEVELS.warning);
                    deferred.reject(msg);
                    deferred.resolve();
                    return;
                }

                CardFlightReader.cardFlight.watchForSwipe(function (data) {
                    console.log('[CardFlightReader] Succeed watchForSwipe: ' + JSON.stringify(data));
                }, function (error) {
                    var msg = '[CardFlightReader] Failed watchForSwipe: ' + error;
                    console.log(msg);
                    deferred.reject(msg);
                });

                CardFlightReader.cardFlight.onSwipeDetected(function (data) {
                    // HEADS up, this function is weirdly named -- this doesn't imply a swipe actually happened,
                    // but rather that we are intending to start listening for swipes.
                    //console.log('[CardFlightReader]#onSwipeDetected hook success: ' + JSON.stringify(data));

                }, function() {
                    var msg = '[CardFlightReader]#onSwipeDetected hook failed';
                    //console.log(msg);
                    deferred.reject(msg);
                });

//                CardFlightReader.cardFlight.onReaderFail(function(errorCode) {
//                    // HEADS up, this event does not run on swipe failure -- onCardRead is the function where swipes
//                    // pass or fail.
//                    //console.log('[CardFlightReader]#onReaderFail hook');
//                });

                CardFlightReader.cardFlight.onCardRead(function (cardReadData) {
                    //console.log('[CardFlightReader]#onCardRead hook success');
                    cardReadData = JSON.parse(cardReadData);
                    deferred.resolve(cardReadData);
                }, function (error) {
                    deferred.reject(error);
                });
            }
            else if (window.platform == 'Web') {
                //console.log('[CardFlightReader]#beginSwipe() web platform');

                State.showError(errorPluginIsNotInstalled.message, ERROR_LEVELS.debug);
                //console.log("swipe error", errorPluginIsNotInstalled.message);
                deferred.reject(errorPluginIsNotInstalled);
            }
            else {
                //console.log('[CardFlightReader]#beginSwipe() plugin was not found');

                if (window.localStorage && localStorage.debugCardToken) { // set it over console for debug purposes
                    deferred.resolve({
                        cardToken: localStorage.debugCardToken
                    });
                }
                else {
                    console.log(errorNotAvailableAtWeb.message);
                }
            }

            return deferred.promise;
        },

        tokenizeLastSwipe: function () {
            var deferred = $q.defer();
            CardFlightReader.cardFlight.tokenizeLastSwipe(function (data) {
                //console.log('[CardFlightReader]#tokenizeLastSwipe callback success');
                //console.log('[CardFlightReader] tokenizeLastSwipe data ' + JSON.stringify(data));

                deferred.resolve({
                    cardToken: data
                });
            }, function (error) {
                var msg = '[CardFlightReader]#tokenizeLastSwipe callback failed: ' + error;
                //console.log(msg);
                deferred.reject(error);
            });
            return deferred.promise;
        },

        chargeLastSwipe: function (amount) {
            var deferred = $q.defer();
            CardFlightReader.cardFlight.chargeLastSwipe(amount, function (token) {
                //console.log('[CardFlightReader] chargeLastSwipe data ' + JSON.stringify(token));

                deferred.resolve({
                    chargeToken: token
                });
            }, function (error) {
                //console.log('[CardFlightReader]#chargeLastSwipe callback failed: ' + error);
                deferred.reject(error);
            });
            return deferred.promise;
        },

        refundCharge: function (chargeToken, amount) {
            var deferred = $q.defer();
            CardFlightReader.cardFlight.refundCharge(chargeToken, amount, function (token) {
                //console.log('[CardFlightReader] refundCharge data ' + JSON.stringify(token));

                deferred.resolve({
                    chargeToken: token
                });
            }, function (error) {
                //console.log('[CardFlightReader]#refundCharge callback failed: ' + error);
                deferred.reject(error);
            });
            return deferred.promise;
        },

        /**
         * @returns {Q.promise}
         */
        stopSwipe: function () {
            console.log('[CardFlightReader]#stopSwipe()');
            var deferred = $q.defer();

            if (CardFlightReader.cardFlight) {
                CardFlightReader.cardFlight.stopSwipe();
                deferred.resolve();
            }
            else if (window.platform == 'Web') {
                //console.log('[CardFlightReader]#stopSwipe() web platform');
                State.showMessage(errorPluginIsNotInstalled.message, ERROR_LEVELS.debug);
                //console.log("stopSwipe error", errorPluginIsNotInstalled.message);
                deferred.reject(errorPluginIsNotInstalled);
            }
            else {
                console.log('[CardFlightReader]#stopSwipe() plugin was not found');
                //console.log(errorNotAvailableAtWeb.message);
            }

            return deferred.promise;
        }
    };

    //DEV_PANEL.CardFlightReader = CardFlightReader;

    console.log('[CardFlightReader] Factory loaded');
    //console.log('[CardFlightReader]#Status: isConnected = false, isReady = false');
    return CardFlightReader;
});
